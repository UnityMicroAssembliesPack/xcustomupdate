﻿using UnityEngine;

[ExecuteAlways]
public class XCustomUpdateManager : XManager
{
    internal void registerUpdater(XCustomUpdater inUpdater) {
        XUtils.check(inUpdater);
        XUtils.check(!_updaters.contains(inUpdater));

        _updaters_deferredAdd.add(inUpdater);
    }

    private void LateUpdate() {
        performDeferredActions();

        _updaters.iterateWithRemove((XCustomUpdater inUpdater) =>{
            if (!inUpdater.isValid()) return true;
            performUpdateForUpdater(inUpdater);
            return false;
        }, true);

        _updaters.iterateWithRemove((XCustomUpdater inUpdater) => {
            if (!inUpdater.isValid()) return true;
            inUpdater.isUpdated = false;
            return false;
        }, true);

        performDeferredActions();
    }

    private void performDeferredActions() {
        if (!_updaters_deferredAdd.isEmpty()) {
            _updaters.addAll(_updaters_deferredAdd);
            _updaters_deferredAdd.clear(false);
        }
    }

    private void performUpdateForUpdater(XCustomUpdater inUpdater) {
        //TODO: Use stack data structure (or even some specific data structure)
        if (inUpdater.isUpdated) return;

        _updateData_globalStack.add(inUpdater);

        _updateData_localStackA.clear(false);
        inUpdater.collectDependnecies(_updateData_localStackA);

        while (true) {
            foreach (XCustomUpdater theCustomUpdater in _updateData_localStackA) {
                XUtils.check(!_updateData_globalStack.contains(theCustomUpdater), "Cycle dependency detected");

                if (theCustomUpdater.isUpdated) continue;
                theCustomUpdater.collectDependnecies(_updateData_localStackB);
            }
            if (_updateData_localStackB.isEmpty()) break;

            _updateData_globalStack.addAll(_updateData_localStackB);

            _updateData_localStackA.assignFrom(_updateData_localStackB, true);
            _updateData_localStackB.clear();
        }

        while (!_updateData_globalStack.isEmpty()) {
            XCustomUpdater theUpdater = _updateData_globalStack.removeLastElement();
            theUpdater.perforUpdate();
            theUpdater.isUpdated = true;
        }
    }

    private FastArray<XCustomUpdater> _updaters = new FastArray<XCustomUpdater>();
    private FastArray<XCustomUpdater> _updaters_deferredAdd = new FastArray<XCustomUpdater>();

    private FastArray<XCustomUpdater> _updateData_globalStack = new FastArray<XCustomUpdater>();
    private FastArray<XCustomUpdater> _updateData_localStackA = new FastArray<XCustomUpdater>();
    private FastArray<XCustomUpdater> _updateData_localStackB = new FastArray<XCustomUpdater>();
}
