﻿public class XCustomUpdater
{
    public XCustomUpdater(System.Action inUpdateFunction,
        System.Func<bool> inIsValidFunction = null,
        System.Action<FastArray<XCustomUpdater>> inDependenciesCollectFunction = null)
    {
        XInfrastructure.getManager<XCustomUpdateManager>().registerUpdater(this);

        _updateFunction = XUtils.verify(inUpdateFunction);
        _isValidFunction = inIsValidFunction;
        _dependenciesCollectFunction = inDependenciesCollectFunction;
    }

    internal bool isValid() {
        return XUtils.isValid(_isValidFunction) ? _isValidFunction.Invoke() : true;
    }

    internal void collectDependnecies(FastArray<XCustomUpdater> outDependencies) {
        _dependenciesCollectFunction?.Invoke(outDependencies);
    }

    internal void perforUpdate() {
        _updateFunction.Invoke();
    }

    private System.Action _updateFunction = null;
    private System.Func<bool> _isValidFunction = null;
    private System.Action<FastArray<XCustomUpdater>> _dependenciesCollectFunction = null;

    internal bool isUpdated = false;
}
